def get_page_metadata(soup):
    """
    Metadata from soup variable
    :param soup: BeautifulSoup type object
    :return:  JSON type object
    """
    meta_data = []
    for attr, value in map(lambda x: x.attrs, soup.findAll('meta')):
        meta_data.append({str(attr): str(value)})
    return meta_data


def get_page_links(soup):
    """
    Finds all a tags with href attribute initialised from soup variable.
    :param soup: BeautifulSoup type object
    :return: JSON type object {'index': <int>,
                                'link':<string>}
    """
    page_links = []
    links = map(lambda y: y.get('href'),
                filter(lambda x: (x.get('href') is not None) and (x.get('href') is not ''),
                       soup.findAll('a')))

    for idx, link in enumerate(links):
        page_links.append({'index': idx, 'link': link})

    return page_links
