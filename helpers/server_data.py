import datetime


class ServerData:
    def __init__(self):
        self.count = 0
        self.uptime_start = datetime.datetime.now()
        self.links_found_amount = 0

    def counter(self):
        self.count += 1

    def get_uptime(self):
        return str(datetime.datetime.now() - self.uptime_start)

    def increment_links_found(self, amount):
        self.links_found_amount += amount

    def get_links_found_amount_string(self):
        return self.links_found_amount
