import json
import unittest
import urllib2


class TestMethods(unittest.TestCase):
    host = 'http://0.0.0.0:5555/'
    host_clean = '0.0.0.0:5555\/'
    server_data = 'server_data/'
    system_time = 'system_time/'
    page_details = 'page_details/?url='
    testing_template = 'testing_template/'

    def test_metadata_amount_is_correct(self):
        url = self.host + self.page_details + 'neti.ee'
        content = json.load(urllib2.urlopen(url))
        assert len(content['meta_data']) != 0

    def test_links_amount_is_correct(self):
        url = self.host + self.page_details + 'neti.ee'
        content = json.load(urllib2.urlopen(url))
        assert len(content['page_links']) != 0

    def test_page_title_is_correct(self):
        url = self.host + self.page_details + 'neti.ee'
        content = json.load(urllib2.urlopen(url))
        assert str(content['page_title']) == 'NETI - Eesti Interneti Kataloog ja Otsingus&uuml;steem'

    def test_page_links_count_is_incremented(self):
        # Open and save the server data already found links count
        url = self.host + self.server_data
        content = json.load(urllib2.urlopen(url))
        found_links_amount = content['found_links_amount']

        # Open page detais to increment found links amount and save the amount
        url = self.host + self.page_details + 'neti.ee'
        content = json.load(urllib2.urlopen(url))
        found_links_count = len(content['page_links'])

        # Check if amount is incremented by correct amount
        url = self.host + self.server_data
        content = json.load(urllib2.urlopen(url))
        assert found_links_amount + found_links_count == content['found_links_amount']


if __name__ == '__main__':
    unittest.main()
