import datetime
import rfc3339
import traceback
import urllib2
from collections import OrderedDict

from BeautifulSoup import BeautifulSoup
from flask import Flask, jsonify, request

from helpers.server_data import ServerData
from helpers.tag_data import *

app = Flask(__name__)
server_data = ServerData()


@app.route('/page_details/', methods=['GET'])
def get_page_details():
    """
    UrlParameters:
        url = string of url (neti.ee), ?url=neti.ee
        
    :return: json {'page_title':string,
                   'meta_data':[{'attr':string}...],
                   'page_links':[{'attr':string}...]
                   }
    """
    server_data.counter()
    url = request.args.get('url')
    if url is None:
        return 'NO url'

    if (url[:7] != 'http://') and (url[:9] != 'https://'):
        url = 'http://{0}'.format(url)
    page_data = OrderedDict()
    try:
        page = urllib2.urlopen(url)
        soup = BeautifulSoup(page)

        page_data['page_title'] = soup.title.string
        page_data['meta_data'] = get_page_metadata(soup)

        page_links = get_page_links(soup)
        server_data.increment_links_found(len(page_links))
        page_data['page_links'] = page_links

    except urllib2.URLError:
        return 'Can not open url'
    except:
        traceback.print_exc()
        return 'Error: check the logs'
    return jsonify(page_data)


@app.route('/system_time/', methods=['GET'])
def get_system_time():
    """
    Returns time of server system in format RFC3339
    :return: JSON type object {"time": "2017-05-11T15:35:30+03:00", 
                               "time_with_utc": "2017-05-11T18:35:30Z"}
    """
    server_data.counter()

    return jsonify({'time': rfc3339.rfc3339(datetime.datetime.now()),
                    'time_with_utc': rfc3339.rfc3339(datetime.datetime.now(), utc=True)})


@app.route('/server_data/', methods=['GET'])
def get_server_data():
    """
    Return monitored Server data in JSON format
    :return: JSON type Object {'found_links_amount':int,
                               'request_count':int,
                               'server_uptime':datetime]
                               }
    """
    server_data.counter()

    return jsonify({'request_count': server_data.count,
                    'server_uptime': server_data.get_uptime(),
                    'found_links_amount': server_data.get_links_found_amount_string()
                    })


if __name__ == '__main__':
    app.run(host='localhost', port=5555, debug=True)
