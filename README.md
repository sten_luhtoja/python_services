Libraries used:
    flask
    BeautifulSoup
    rfc3339
    urllib2
    unittest

RESTful API services:

1. Page details service -http://localhost:5555/page_details/?url=url

    1.1 Test cases:
        curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:5555/page_details/?url=neti.ee

2. System time in RFC3339 Format - http://localhost:5555/system_time/

3. Application monitoring - http://localhost:5555/server_data/